using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoMove : MonoBehaviour
{
    public GameObject spawn1;
    public GameObject spawn2;
    Vector3 ShootDirection;
    void Start()
    {
        
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            ShootDirection = new Vector3(
            Input.mousePosition.x - 400f,
            Input.mousePosition.y - 200, 0
            );

            ShootDirection -= gameObject.transform.position;

            GameObject bullet = Instantiate(
                spawn1,
                gameObject.transform.position,
                gameObject.transform.rotation
                );
            bullet.GetComponent<Rigidbody2D>().velocity = ShootDirection/10;
        }

        if (Input.GetButtonDown("Fire2"))
        {
            ShootDirection = new Vector3(
            Input.mousePosition.x - 400f,
            Input.mousePosition.y - 200, 0
            );

            ShootDirection -= gameObject.transform.position;

            GameObject bullet = Instantiate(
                spawn2,
                gameObject.transform.position,
                gameObject.transform.rotation
                );
            bullet.GetComponent<Rigidbody2D>().velocity = ShootDirection / 10;
        }

    }
}
